<!--Database Connectiong String File Inclluded-->
<?php 
      session_start();
      include 'include/config.php';
	  include 'include/sessionchecker.php';
?>  

<!DOCTYPE html>
<html lang="en">

<!--head-->
<head>
<title>Edit Account | LUXURY GIFTS</title>
<?php include 'include/head.php';?>    
</head>
<!--/head-->

<body>
	 <!--header-->
	<header id="header">
    
    <!--header Top-->
    <?php include 'include/headertop.php';?> 
    <!--/header Top-->
	     
        <!--header-Navigation--> 
		<div class="header-bottom">
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="index.php">Home</a></li>
								
                               
                                <li class="dropdown"><a href="#">Women's<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="products.php?var=Women Clothing">Clothing</a></li>
										<li><a href="products.php?var=Women Shoes">Shoes</a></li>
                                        <li><a href="products.php?var=Women Accessories">Accessories</a></li>
                                    </ul>
                                </li> 
                                
                                <li class="dropdown"><a href="#">Men's<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="products.php?var=Men Clothing">Clothing</a></li>
                                        <li><a href="products.php?var=Men Shoes">Shoes</a></li>
										<li><a href="products.php?var=Men Accessories">Accessories</a></li>
                                    </ul>
                                </li> 
                                 <li class="dropdown"><a href="#">Mobile&Tablets <i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="products.php?var=Phones">Phones</a></li>
										<li><a href="products.php?var=Tablets">Tablets</a></li> 
										
                                    </ul>
                                </li> 
                                <li><a href="products.php?var=Computers">Computers</a></li>
                                <li><a href="products.php?var=Laptops">Laptops</a></li>
								<li><a href="products.php?var=Kids">Kids</a></li>
								<li><a href="products.php?var=Health&Beauty">Health&Beauty</a></li>
                                
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="search_box pull-right">
							 <form id="search" action="search.php" method="post">
							<input type="text" id="search" name="search" placeholder="Search by Name Or Category"/>
                            </form>
						</div>
					</div>
				</div>
			</div>
		</div>
    
    <!--/header-Navigation End-->
	</header>
    <!--/header-->
	
	<section id="form"><!--form-->
		<div class="container">
			<div class="row">
				
						
							<h3>						
							 Account Edit</h3>
                            
                              <?php
				$id=$_GET['id'];
	
	$results = $mysqli->query("select * from users  where id='$id' ");	
    if ($results) { 
	
        //fetch results set as object and output HTML
        while($obj = $results->fetch_object())
        {
			?>
							<div class="form-two">
								<form id="signup" action="accountupdate.php" method="post" onsubmit="return validateForm()">
									<input type="email" name="email" id="email" value="<?php echo $obj->email; ?>" placeholder="Email*">
                                    
                                    
									<input type="text" name="title" id="title"value="<?php echo $obj->title; ?>" placeholder="Title">
                                     
									<input type="text" name="firstname" id="firstname" value="<?php echo $obj->firstname; ?> " placeholder="First Name *">
                                    
									
									<input type="text" name="lastname" id="lastname" value="<?php echo $obj->lastname; ?>" placeholder="Last Name *">
                                    
                                    <input type="text" name="username" id="username" value="<?php echo $obj->username; ?>" placeholder="Username *">
                                      <label>
<?php 
if (isset($_GET['registration']) && $_GET['registration']=='unavailable')
echo "<p style='color:red;'>Username is already Exits Try Again</p>";
?>
</label>
                                    <input type="password" name="password" id="password" value="<?php echo $obj->password; ?>" placeholder="Password *">                                  
									
								
								
                                <input type="text" name="add1" id="add1"value="<?php echo $obj->add1; ?>" placeholder="Address 1 *">
                                 
									<input type="text" name="add2"
                                    id="add2" value="<?php echo $obj->add2; ?>" placeholder="Address 2">
                                    
									<input type="text" name="postcode"
                                    id="postcode" value="<?php echo $obj->postcode; ?>" placeholder="Zip / Postal Code *">
                                     
									
									<select id="state" name="state">
										<option value="<?php echo $obj->state; ?>"><?php echo $obj->state; ?></option>
										<option value="Bedfordshire">Bedfordshire</option>
										<option value="Berkshire">Berkshire</option>
                                        <option value="Buckinghamshire ">  Buckinghamshire</option>
                                        <option value=" Cheshire "> Cheshire  </option>
                                        <option value="Derbyshire  "> Derbyshire  </option>
                                        <option value=" Durham ">  Durham </option>
                                        <option value=" Essex "> Essex  </option>
                                        <option value="  Gloucestershire">Gloucestershire   </option>
                                        <option value=" Hampshire "> Hampshire  </option>
                                        <option value=" Hertfordshire ">Hertfordshire   </option>
                                        <option value=" Huntingdonshire "> Huntingdonshire  </option>
                                        <option value=" Kent "> Kent  </option>
                                        <option value=" Lancashire "> Lancashire  </option>
                                        <option value="  Leicestershire">Leicestershire   </option>
                                        <option value=" Lincolnshire ">Lincolnshire   </option>
                                        <option value=" Middlesex "> Middlesex  </option>
                                        <option value=" Norfolk ">Norfolk   </option>
                                        <option value=" Northamptonshire "> Northamptonshire  </option>
                                        <option value="Northumberland  ">Northumberland   </option>
                                        <option value=" Nottinghamshire "> Nottinghamshire  </option>
                                        <option value=" Oxfordshire "> Oxfordshire  </option>
                                        <option value=" Rutland ">Rutland   </option>
                                        <option value="Shropshire  "> Shropshire  </option>
                                        <option value="Somerset ">Somerset   </option>
                                        <option value=" Staffordshire "> Staffordshire  </option>
                                        <option value=" Suffolk "> Suffolk  </option>
                                        <option value="Surrey  "> Surrey  </option>
                                        <option value=" Sussex "> Sussex  </option>
                                        <option value="Warwickshire"> Warwickshire  </option>
                                        <option value=" Westmoreland ">  Westmoreland </option>
                                        <option value="Wiltshire  "> Wiltshire  </option>
                                        <option value=" Worcestershire "> Worcestershire  </option>
                                        <option value=" Yorkshire  "> Yorkshire   </option>
                                        
										
										
									</select>
                                   
									
									<input type="text" name="phone" id="phone" value="<?php echo $obj->phone; ?>" placeholder="Phone *">
                                    
									<input type="text" name="mobile" id="mobile" value="<?php echo $obj->mobile; ?>" placeholder="Mobile Phone *">
						<input type="hidden" id="id" name="id" value="<?php echo $obj->id; ?>" >
                         		
                                    <button type="submit" id="signupbtn">Update</button>
								</form>
							
					
                      <?php   
                    }
    
                    }
	               ?>
                    	
					</div>
				
				</div>
			</div>
		</div>
	</section><!--/form-->
	
	
	        <!--Footer-->
            <?php include 'include/footer.php';?> 
            <!--/Footer-->
<script src="js/formvalidate.js"></script>   
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>