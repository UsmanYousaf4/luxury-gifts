<?php
 /* Following fucntion will start rthe session automatically after user login */
session_start();

 /* inlcuded configration file to  connect the database  */
include 'include/config.php';

 /* Session checker file included to check whether the user session exits or not */
include 'include/sessionchecker.php';

?>
<!DOCTYPE html>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>LUXURY GIFTS ADMINISTRATION</title>
<link rel="stylesheet" href="css/style.default.css" type="text/css" />
<link rel="stylesheet" href="css/responsive-tables.css">
<script type="text/javascript" src="js/modernizr.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/responsive-tables.js"></script>
</head>

<body>

<div class="mainwrapper">
 <!--header-->   

<?php include 'include/header.php';?>    

<!--/header-->

<div class="leftpanel">
        
        <div class="leftmenu">        
            <ul class="nav nav-tabs nav-stacked">
            	<li class="nav-header">Navigation</li>
                <li><a href="dashboard.php"><span class="iconfa-laptop"></span> Dashboard</a></li>
                <li><a href="viewproducts.php"><span class="iconfa-hand-up"></span> View Products</a></li>
                  
                <li class="active"><a href="addproducts.php"><span class="iconfa-picture"></span> Add Product</a></li>
                <li><a href="viewreviews.php"><span class="iconfa-envelope"></span> Product Reviews</a></li>
                <li><a href="vieworders.php"><span class="iconfa-font"></span> View Orders</a></li>
                <li><a href="viewusers.php"><span class="iconfa-signal"></span> View Users</a></li>
                <li><a href="viewmessages.php"><span class="iconfa-envelope"></span> View Messages</a></li>
                <li><a href="viewpages.php"><span class="iconfa-envelope"></span> View Pages</a></li>               
                <li><a href="email.php"><span class="iconfa-envelope"></span> Send Email</a></li>
                <li><a href="seo.php"><span class="iconfa-envelope"></span> Manage SEO</a></li>
                <li><a href="socialmedia.php"><span class="iconfa-envelope"></span> Social Media</a></li>
                
                
                    </ul>
                </li>
            </ul>
        </div><!--leftmenu-->
        
    </div><!-- leftpanel -->

 <!--right panel-->   

<?php include 'include/rightpanel.php';?>    

<!--/right panel-->
        
        <div class="maincontent">
            <div class="maincontentinner">
                <div class="widgetbox box-inverse">
                <h4 class="widgettitle">Add New Products</h4>
                <div class="widgetcontent wc1">
                
                    <form id="form1" class="stdform" method="post" action="addproductquery.php" enctype="multipart/form-data" >
                            <div class="par control-group">
                                    <label class="control-label" for="firstname">Title</label>
                                <div class="controls"><input type="text" name="title" id="title" class="input-large" /></div>
                            </div>
                            
                            <div class="control-group">
                                    <label class="control-label" for="lastname">Category</label>
                                <div class="controls">
                                
                                <select name="category" id="category">
                                <option value="">Select</option>
                                <option value="Phones">Phones</option>                                
                                <option value="Tablets">Tablets</option>                                
                                 <option value="Men Clothing">Men Clothing</option>
                                 <option value="Men Shoes">Men Shoes</option>
                                 <option value="Men Accessories">Men Accessories</option>
                                 <option value="Women Clothing">Women Clothing</option>
                                 <option value="Women Shoes">Women Shoes</option>
                                 <option value="Women Accessories">Women Accessories</option>
                                 <option value="Computers">Computers</option>
                                 <option value="Laptops">Laptops</option>
                                 <option value="Kids">Kids</option>
                                 <option value="Health & Beauty">Health & Beauty</option>
                                 
</select>
                                </div>
                            </div>
                            
                            <div class="control-group">
                                    <label class="control-label" for="lastname">Availability</label>
                                <div class="controls">
                                
                                <select name="availability" id="availability">
                                 <option value="">Select</option>
                                <option value="In stock">In stock</option>
                                <option value="Out of stock">Out of stock</option>
                                
</select>
                                </div>
                            </div>
                            
                            <div class="control-group">
                                    <label class="control-label" for="lastname">Quantity</label>
                                <div class="controls">
                                
                                <select name="quantity" id="quantity">
                                 <option value="">Select</option>
                                <option value="10">10</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                                <option value="150">150</option>
</select>
                                </div>
                            </div>
                            
                            <div class="par control-group">
                                    <label class="control-label" for="firstname">Price</label>
                                <div class="controls"><input type="text" name="price" id="price" class="input-large" /></div>
                            </div>
                            
                             <div class="par control-group">
                                    <label class="control-label" for="firstname">Points</label>
                                <div class="controls"><input type="text" name="points" id="points" class="input-large" /></div>
                            </div>
                            
                            <div class="par control-group">
                                    <label class="control-label" for="firstname">Upload Image</label>
                                <div class="controls"><input type="file" name="filep" id="filep" class="input-large" /></div>
                            </div>
                            
                            
                                                           
                            <div class="par control-group">
                                    <label class="control-label" for="location">Description</label>
                                <div class="controls"><textarea cols="20" rows="3" name="des" class="input-xxlarge" id="des"></textarea></div> 
                            </div>
                                                    
                            <p class="stdformbutton">
                            <input type="submit" class="btn btn-primary" value="Add New Product">
                                    
                            </p>
                    </form>
                </div><!--widgetcontent-->
            </div><!--row-fluid-->
                
                <!--footer-->   

<?php include 'include/footer.php';?>    

<!--/footer-->
                
            </div><!--maincontentinner-->
        </div><!--maincontent-->
        
    </div><!--rightpanel-->
    
</div><!--mainwrapper-->

</body>
</html>
