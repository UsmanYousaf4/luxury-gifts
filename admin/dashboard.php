<?php
 /* Following fucntion will start rthe session automatically after user login */
session_start();

 /* inlcuded configration file to  connect the database  */
include 'include/config.php';

 /* Session checker file included to check whether the user session exits or not */
include 'include/sessionchecker.php';

?>
<!DOCTYPE html>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>LUXURY GIFTS ADMINISTRATION</title>
<link rel="stylesheet" href="css/style.default.css" type="text/css" />
<link rel="stylesheet" href="css/responsive-tables.css">
<script type="text/javascript" src="js/modernizr.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/responsive-tables.js"></script>
</head>

<body>

<div class="mainwrapper">
 <!--header-->   

<?php include 'include/header.php';?>    

<!--/header-->

<div class="leftpanel">
        
        <div class="leftmenu">        
            <ul class="nav nav-tabs nav-stacked">
            	<li class="nav-header">Navigation</li>
                <li class="active"><a href="dashboard.php"><span class="iconfa-laptop"></span> Dashboard</a></li>
                <li><a href="viewproducts.php"><span class="iconfa-hand-up"></span> View Products</a></li>
                  
                <li><a href="addproducts.php"><span class="iconfa-picture"></span> Add Product</a></li>
                <li><a href="viewreviews.php"><span class="iconfa-envelope"></span> Product Reviews</a></li>
                <li><a href="vieworders.php"><span class="iconfa-font"></span> View Orders</a></li>
                <li><a href="viewusers.php"><span class="iconfa-signal"></span> View Users</a></li>
                <li><a href="viewmessages.php"><span class="iconfa-envelope"></span> View Messages</a></li>
                <li><a href="viewpages.php"><span class="iconfa-envelope"></span> View Pages</a></li>
                <li><a href="email.php"><span class="iconfa-envelope"></span> Send Email</a></li>
                <li><a href="seo.php"><span class="iconfa-envelope"></span> Manage SEO</a></li>
                <li><a href="socialmedia.php"><span class="iconfa-envelope"></span> Social Media</a></li>
                
                
                    </ul>
                </li>
            </ul>
        </div><!--leftmenu-->
        
    </div><!-- leftpanel -->

 <!--right panel-->   

<?php include 'include/rightpanel.php';?>    

<!--/right panel-->
        
        <div class="maincontent">
            <div class="maincontentinner">
            <h3>Dashboard</h3>
                <div class="row-fluid">
                    <div id="dashboard-left" class="span8">
                        
                        
                        <ul class="shortcuts">
                        <li class="products">
                                <a href="viewproducts.php">
                                    <span class="shortcuts-icon iconsi-cart"></span>
                                    <span class="shortcuts-label">View Products</span>
                                </a>
                            </li>
                             <li class="archive">
                                <a href="addproducts.php">
                                    <span class="shortcuts-icon iconsi-archive"></span>
                                    <span class="shortcuts-label">Add Product</span>
                                </a>
                            </li>
                            <li class="events">
                                <a href="vieworders.php">
                                    <span class="shortcuts-icon iconsi-event"></span>
                                    <span class="shortcuts-label">View Orders</span>
                                </a>
                            </li>
                            
                           
                            <li class="help">
                                <a href="viewusers.php">
                                    <span class="shortcuts-icon iconsi-help"></span>
                                    <span class="shortcuts-label">View Users</span>
                                </a>
                            </li>
                            <li class="last images">
                                <a href="viewmessages.php">
                                    <span class="shortcuts-icon iconsi-images"></span>
                                    <span class="shortcuts-label"> Messages</span>
                                </a>
                            </li>
                             <li class="last images">
                                <a href="email.php">
                                    <span class="shortcuts-icon iconsi-images"></span>
                                    <span class="shortcuts-label"> Send Email</span>
                                </a>
                            </li>
                            <li class="help">
                                <a href="seo.php">
                                    <span class="shortcuts-icon iconsi-seo"></span>
                                    <span class="shortcuts-label"> Manage SEO</span>
                                </a>
                            </li>
                            <li class="pages">
                                <a href="viewpages.php">
                                    <span class="shortcuts-icon iconsi-pages"></span>
                                    <span class="shortcuts-label"> View Pages</span>
                                </a>
                            </li>
                             <li class="review">
                                <a href="viewreviews.php">
                                    <span class="shortcuts-icon iconsi-review"></span>
                                    <span class="shortcuts-label"> View Reviews</span>
                                </a>
                            </li>
                            <li class="help">
                                <a href="socialmedia.php">
                                    <span class="shortcuts-icon iconsi-help"></span>
                                    <span class="shortcuts-label">Social Media</span>
                                </a>
                            </li>
                        </ul>
                        
                       
                        
                      
                        <br />
                        
                        
                    </div><!--span8--><!--span4-->
                </div><!--row-fluid-->
                
 <!--footer-->   

<?php include 'include/footer.php';?>    

<!--/footer-->
                
            </div><!--maincontentinner-->
        </div><!--maincontent-->
        
    </div><!--rightpanel-->
    
</div><!--mainwrapper-->

</body>
</html>
