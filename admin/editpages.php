<?php
 /* Following fucntion will start rthe session automatically after user login */
session_start();

 /* inlcuded configration file to  connect the database  */
include 'include/config.php';

 /* Session checker file included to check whether the user session exits or not */
include 'include/sessionchecker.php';

?>
<!DOCTYPE html>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>LUXURY GIFTS ADMINISTRATION</title>
<link rel="stylesheet" href="css/style.default.css" type="text/css" />
<link rel="stylesheet" href="css/responsive-tables.css">

<script type="text/javascript" src="js/tinymce/jscripts/tiny_mce/tiny_mce.js" >
</script>
<script type="text/javascript">
tinyMCE.init({
        mode : "textareas",
        theme : "advanced",
        plugins : "emotions,spellchecker,advhr,insertdatetime,preview", 
                
        // Theme options - button# indicated the row# only
        theme_advanced_buttons1 : "newdocument,|,bold,italic,underline,|,justifyleft,justifycenter,justifyright,fontselect,fontsizeselect,formatselect",
        theme_advanced_buttons2 : "cut,copy,paste,|,bullist,numlist,|,outdent,indent,|,undo,redo,|,link,unlink,anchor,image,|,code,preview,|,forecolor,backcolor",
        theme_advanced_buttons3 : "insertdate,inserttime,|,spellchecker,advhr,,removeformat,|,sub,sup,|,charmap,emotions",      
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true
});
</script>
<script type="text/javascript" src="../../ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<script type="text/javascript" src="js/modernizr.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/responsive-tables.js"></script>
</head>

<body>

<div class="mainwrapper">
 <!--header-->   

<?php include 'include/header.php';?>    

<!--/header-->

<div class="leftpanel">
        
        <div class="leftmenu">        
            <ul class="nav nav-tabs nav-stacked">
            	<li class="nav-header">Navigation</li>
                <li class="active"><a href="dashboard.php"><span class="iconfa-laptop"></span> Dashboard</a></li>
                <li><a href="viewproducts.php"><span class="iconfa-hand-up"></span> View Products</a></li>
                  
                <li><a href="addproducts.php"><span class="iconfa-picture"></span> Add Product</a></li>
                <li><a href="viewreviews.php"><span class="iconfa-envelope"></span> Product Reviews</a></li>
                <li><a href="vieworders.php"><span class="iconfa-font"></span> View Orders</a></li>
                <li><a href="viewusers.php"><span class="iconfa-signal"></span> View Users</a></li>
                <li><a href="viewmessages.php"><span class="iconfa-envelope"></span> View Messages</a></li>
                <li class="active"><a href="viewpages.php"><span class="iconfa-envelope"></span> View Pages</a></li>
                <li><a href="email.php"><span class="iconfa-envelope"></span> Send Email</a></li>
                <li><a href="seo.php"><span class="iconfa-envelope"></span> Manage SEO</a></li>
                <li><a href="socialmedia.php"><span class="iconfa-envelope"></span> Social Media</a></li>
                
                
                    </ul>
                </li>
            </ul>
        </div><!--leftmenu-->
        
    </div><!-- leftpanel -->

 <!--right panel-->   

<?php include 'include/rightpanel.php';?>    

<!--/right panel-->
        
        <div class="maincontent">
            <div class="maincontentinner">
                <div class="widgetbox box-inverse">
                <h4 class="widgettitle">Update Products Information</h4>
                <div class="widgetcontent wc1">
                   <?php
				
	$id=$_GET['id'];
	$results = $mysqli->query("select * from pages where id= '$id' ");	
    if ($results) { 
	
        //fetch results set as object and output HTML
        while($obj = $results->fetch_object())
        {
			?>
                
                
                
                    <form id="form1" class="stdform" method="post" action="add1.php" enctype="multipart/form-data" >
                            <div class="par control-group">
                                    <label class="control-label" for="firstname">Title</label>
                                <div class="controls"><input type="text" name="title" id="title" class="input-large" value="<?php echo $obj->title; ?>" /></div>
                            </div>
                            
                           
                             <div class="par control-group">
                                    <label class="control-label" for="firstname">Content</label>
                                <div class="controls"><textarea name="content" cols="100" rows="25" > <?php echo $obj->content; ?> </textarea></div>
                            </div>
                            
                            
                            
                            
                           
                            
                            
                                                           
                            
                                                    
                            <p class="stdformbutton">
                            <input type="hidden" id="id" name="id" value="<?php echo $obj->id; ?>" >
                            <input type="submit" class="btn btn-primary" value="Update Page">
                                    
                            </p>
                    </form>
                    
                     <?php   
                    }
    
                    }
	               ?>
                    
                    
                </div><!--widgetcontent-->
            </div><!--row-fluid-->
                
                <!--footer-->   

<?php include 'include/footer.php';?>    

<!--/footer-->
                
            </div><!--maincontentinner-->
        </div><!--maincontent-->
        
    </div><!--rightpanel-->
    
</div><!--mainwrapper-->

</body>
</html>
