<?php

/* Following statement will check the user session exits or not. if session does not exits it will redirect the user to the login page */

if(empty($_SESSION['admin'])) 
    { 
        header("Location: index.php");       
       
        die("Redirecting to index.php"); 
    }
?>