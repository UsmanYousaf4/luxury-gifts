<?php
 /* Following fucntion will start rthe session automatically after user login */
session_start();

 /* inlcuded configration file to  connect the database  */
include 'include/config.php';

 /* Session checker file included to check whether the user session exits or not */
include 'include/sessionchecker.php';

?>

<!DOCTYPE html>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>LUXURY GIFTS ADMINISTRATION</title>
<link rel="stylesheet" href="css/style.default.css" type="text/css" />
<link rel="stylesheet" href="css/responsive-tables.css">
<script type="text/javascript" src="js/modernizr.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/responsive-tables.js"></script>
</head>

<body>

<div class="mainwrapper">
 <!--header-->   

<?php include 'include/header.php';?>    

<!--/header-->

<div class="leftpanel">
        
        <div class="leftmenu">        
            <ul class="nav nav-tabs nav-stacked">
            	<li class="nav-header">Navigation</li>
                <li><a href="dashboard.php"><span class="iconfa-laptop"></span> Dashboard</a></li>
                <li><a href="viewproducts.php"><span class="iconfa-hand-up"></span> View Products</a></li>
                  
                <li><a href="addproducts.php"><span class="iconfa-picture"></span> Add Product</a></li>
                <li><a href="viewreviews.php"><span class="iconfa-envelope"></span> Product Reviews</a></li>
                <li><a href="vieworders.php"><span class="iconfa-font"></span> View Orders</a></li>
                <li><a href="viewusers.php"><span class="iconfa-signal"></span> View Users</a></li>
                <li><a href="viewmessages.php"><span class="iconfa-envelope"></span> View Messages</a></li>
                  <li><a href="viewpages.php"><span class="iconfa-envelope"></span> View Pages</a></li>
                <li><a href="email.php"><span class="iconfa-envelope"></span> Send Email</a></li>
                <li class="active"><a href="seo.php"><span class="iconfa-envelope"></span> Manage SEO</a></li>
                <li><a href="socialmedia.php"><span class="iconfa-envelope"></span> Social Media</a></li>
                
                
                
                    </ul>
                </li>
            </ul>
        </div><!--leftmenu-->
        
    </div><!-- leftpanel -->

 <!--right panel-->   

<?php include 'include/rightpanel.php';?>    

<!--/right panel-->
        
        <div class="maincontent">
            <div class="maincontentinner">
                <div class="widgetbox box-inverse">
                <h4 class="widgettitle">Manage SEO </h4>
               
                <div class="widgetcontent wc1">
                
                 <span>
   <?php 
   if(isset($_GET['msg']))
  echo "<p style='color:red'> ".$_GET['msg']." </p>";
  ?>
  </span>
                 <?php
					
	$results = $mysqli->query("select * from seo");	
    if ($results) { 
	
        //fetch results set as object and output HTML
        while($obj = $results->fetch_object())
        {
			?>
                
                
                    <form id="form1" class="stdform" method="post" action="seoquery.php">
                           
                            <div class="par control-group">
                                    <label class="control-label" for="location">Keywords</label>
                                <div class="controls"><textarea cols="20" rows="3" name="keywords" class="input-xxlarge" id="keywords"><?php echo $obj->keywords; ?></textarea></div> 
                            </div>
                            <div class="par control-group">
                                    <label class="control-label" for="location">Description</label>
                                <div class="controls"><textarea cols="20" rows="3" name="des" class="input-xxlarge" id="des"><?php echo $obj->description; ?></textarea></div> 
                            </div>
                                                    
                            <p class="stdformbutton">
                            <input type="submit" class="btn btn-primary" value="Submit">
                                    
                            </p>
                    </form>
                     <?php   
                    }
    
                    }
	               ?>
                </div><!--widgetcontent-->
            </div><!--row-fluid-->
                
                <!--footer-->   

<?php include 'include/footer.php';?>    

<!--/footer-->
                
            </div><!--maincontentinner-->
        </div><!--maincontent-->
        
    </div><!--rightpanel-->
    
</div><!--mainwrapper-->

</body>
</html>
