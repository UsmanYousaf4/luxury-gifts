<?php
 /* Following fucntion will start rthe session automatically after user login */
session_start();

 /* inlcuded configration file to  connect the database  */
include 'include/config.php';

 /* Session checker file included to check whether the user session exits or not */
include 'include/sessionchecker.php';

?>
<!DOCTYPE html>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>LUXURY GIFTS ADMINISTRATION</title>
<link rel="stylesheet" href="css/style.default.css" type="text/css" />
<link rel="stylesheet" href="css/responsive-tables.css">
<script type="text/javascript" src="js/modernizr.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/responsive-tables.js"></script>
</head>

<body>

<div class="mainwrapper">
 <!--header-->   

<?php include 'include/header.php';?>    

<!--/header-->

<div class="leftpanel">
        
        <div class="leftmenu">        
            <ul class="nav nav-tabs nav-stacked">
            	<li class="nav-header">Navigation</li>
                <li><a href="dashboard.php"><span class="iconfa-laptop"></span> Dashboard</a></li>
                <li><a href="viewproducts.php"><span class="iconfa-hand-up"></span> View Products</a></li>
                  
                <li><a href="addproducts.php"><span class="iconfa-picture"></span> Add Product</a></li>
                <li><a href="viewreviews.php"><span class="iconfa-envelope"></span> Product Reviews</a></li>
                <li class="active"><a href="vieworders.php"><span class="iconfa-font"></span> View Orders</a></li>
                <li><a href="viewusers.php"><span class="iconfa-signal"></span> View Users</a></li>
                <li><a href="viewmessages.php"><span class="iconfa-envelope"></span> View Messages</a></li>
                <li><a href="viewpages.php"><span class="iconfa-envelope"></span> View Pages</a></li>
                <li><a href="email.php"><span class="iconfa-envelope"></span> Send Email</a></li>
                <li><a href="seo.php"><span class="iconfa-envelope"></span> Manage SEO</a></li>
                <li><a href="socialmedia.php"><span class="iconfa-envelope"></span> Social Media</a></li>
                
                
                    </ul>
                </li>
            </ul>
        </div><!--leftmenu-->
        
    </div><!-- leftpanel -->

 <!--right panel-->   

<?php include 'include/rightpanel.php';?>    

<!--/right panel-->
        
        <div class="maincontent">
            <div class="maincontentinner">
              <span>
   <?php 
   if(isset($_GET['msg']))
  echo "<p style='color:red'> ".$_GET['msg']." </p>";
  ?>
  </span>
              <h4 class="widgettitle">Manage Customers Orders</h4>
            	<table class="table table-bordered responsive">
                    <thead>
                    
                        <tr>
                        	<th class="centeralign"><input type="checkbox" class="checkall" /></th>
                            <th>Order ID</th>
                            <th>Customer</th>                            
                            <th>Quantity</th>
                            <th>Reward Points</th>
                            <th>Discount%</th>
                            <th>Discount Amount</th>
                            <th>Total Amount </th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    
                     <?php

    
	$results = $mysqli->query("SELECT * FROM orders");
    if ($results) { 
	
        //fetch results set as object and output HTML
        while($obj = $results->fetch_object())
        {
			
			echo '
                    
                        <tr>
                        	<td class="centeralign"><input type="checkbox" /></td>
                            <td>'.$obj->id.'</td>
                            <td>'.$obj->username.'</td>
                            <td>'.$obj->quantity.'</td>
                            <td>£'.$obj->r_points.'</td>
                            <td>'.$obj->discount.'%</td>
                            <td>'.$obj->discount_p.'£</td>
							<td>'.$obj->order_total.'£</td>
                           
                            <td class="centeralign"><a href="deleteorder.php?id='.$obj->id.'" class="deleterow">Delete</a></td>
                        </tr>
                         ';
        }
    
    }
    ?>  
                        
                        
                    </tbody>
                </table>
             
                    
                
                <!--footer-->   

<?php include 'include/footer.php';?>    

<!--/footer-->
                
            </div><!--maincontentinner-->
        </div><!--maincontent-->
        
    </div><!--rightpanel-->
    
</div><!--mainwrapper-->

</body>
</html>
