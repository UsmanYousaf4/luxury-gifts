<?php
 /* Following fucntion will start rthe session automatically after user login */
session_start();

 /* inlcuded configration file to  connect the database  */
include 'include/config.php';

 /* Session checker file included to check whether the user session exits or not */
include 'include/sessionchecker.php';

?>
<!DOCTYPE html>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>LUXURY GIFTS ADMINISTRATION</title>
<link rel="stylesheet" href="css/style.default.css" type="text/css" />
<link rel="stylesheet" href="css/responsive-tables.css">
<script type="text/javascript" src="js/modernizr.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/responsive-tables.js"></script>
</head>

<body>

<div class="mainwrapper">
 <!--header-->   

<?php include 'include/header.php';?>    

<!--/header-->

<div class="leftpanel">
        
        <div class="leftmenu">        
            <ul class="nav nav-tabs nav-stacked">
            	<li class="nav-header">Navigation</li>
                <li><a href="dashboard.php"><span class="iconfa-laptop"></span> Dashboard</a></li>
                <li class="active"><a href="viewproducts.php"><span class="iconfa-hand-up"></span> View Products</a></li>
                  
                <li><a href="addproducts.php"><span class="iconfa-picture"></span> Add Product</a></li>
                 <li><a href="viewreviews.php"><span class="iconfa-envelope"></span> Product Reviews</a></li>
                <li><a href="vieworders.php"><span class="iconfa-font"></span> View Orders</a></li>
                <li><a href="viewusers.php"><span class="iconfa-signal"></span> View Users</a></li>
                <li><a href="viewmessages.php"><span class="iconfa-envelope"></span> View Messages</a></li>
                <li><a href="viewpages.php"><span class="iconfa-envelope"></span> View Pages</a></li>
                <li><a href="email.php"><span class="iconfa-envelope"></span> Send Email</a></li>
                <li><a href="seo.php"><span class="iconfa-envelope"></span> Manage SEO</a></li>
                <li><a href="socialmedia.php"><span class="iconfa-envelope"></span> Social Media</a></li>
                
                
                    </ul>
                </li>
            </ul>
        </div><!--leftmenu-->
        
    </div><!-- leftpanel -->

 <!--right panel-->   

<?php include 'include/rightpanel.php';?>    

<!--/right panel-->
        
        <div class="maincontent">
            <div class="maincontentinner">
            <h3>Manage Products</h3>
             <span>
   <?php 
   if(isset($_GET['msg']))
  echo "<p style='color:red'> ".$_GET['msg']." </p>";
  ?>
  </span>
   <span>
   <?php 
   if(isset($_GET['msg1']))
  echo "<p style='color:red'> ".$_GET['msg1']." </p>";
  ?>
  </span>
  <span>
   <?php 
   if(isset($_GET['msg4']))
  echo "<p style='color:red'> ".$_GET['msg4']." </p>";
  ?>
  </span>
  
            <?php

    
	$results = $mysqli->query("SELECT * FROM products ORDER BY id DESC");
    if ($results) { 
	
        //fetch results set as object and output HTML
        while($obj = $results->fetch_object())
        {
			echo '
			        
                    <div class="span6">
					<div class="topicpanel">
                       <ul id="slidercontent">
                         
                           
                            <li>
                                <div class="slide_wrap">
                                  	<div class="slide_img"><img src="images/'.$obj->img.'" alt="" /></div>
                                    <div class="slide_content">
                                    	<h3>Title: '.$obj->title.'</h3>
                                        <h4>Category: '.$obj->category.'</h4>
                                        
                                        <h4>Price: £'.$obj->price.'</h4>
                                        
                                        <h4>Quantity: '.$obj->quantity.'</h4>
                                        
                                        <h4>Availability:'.$obj->availability.' </h4>
                                        <h4>Description: '.$obj->des.' </h4>
                                                                                                            <a href="editproducts.php?id='.$obj->id.'">  <p><button class="btn btn-primary">EDIT</button></a>
                                                                                                             <a href="deleteproduct.php?id='.$obj->id.'">  <button class="btn">DELETE</button></p></a>
                                    </div>
                                </div>
                            </li>
                           
                        </ul>
                    </div></div><!--span6--><!--row-fluid-->
                     ';
        }
    
    }
    ?>  
                   
                    
                
                <!--footer-->   

<?php include 'include/footer.php';?>    

<!--/footer-->
                
            </div><!--maincontentinner-->
        </div><!--maincontent-->
        
    </div><!--rightpanel-->
    
</div><!--mainwrapper-->

</body>
</html>
