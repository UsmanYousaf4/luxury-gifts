<?php
session_start();

include 'include/config.php';

?>
<!DOCTYPE html>
<html lang="en">

<!--head-->
<head>
<title>Contact Us | LUXURY GIFTS</title>
<?php include 'include/head.php';?>    
</head>
<!--/head-->

<body>
	 <!--header-->
	<header id="header">
    
    <!--header Top-->
    <?php include 'include/headertop.php';?> 
    <!--/header Top-->
	     
        <!--header-Navigation--> 
		<div class="header-bottom">
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="index.php">Home</a></li>
								
                               
                                <li class="dropdown"><a href="#">Women's<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="products.php?var=Women Clothing">Clothing</a></li>
										<li><a href="products.php?var=Women Shoes">Shoes</a></li>
                                        <li><a href="products.php?var=Women Accessories">Accessories</a></li>
                                    </ul>
                                </li> 
                                
                                <li class="dropdown"><a href="#">Men's<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="products.php?var=Men Clothing">Clothing</a></li>
                                        <li><a href="products.php?var=Men Shoes">Shoes</a></li>
										<li><a href="products.php?var=Men Accessories">Accessories</a></li>
                                    </ul>
                                </li> 
                                 <li class="dropdown"><a href="#">Mobile&Tablets <i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="products.php?var=Phones">Phones</a></li>
										<li><a href="products.php?var=Tablets">Tablets</a></li> 
										
                                    </ul>
                                </li> 
                                <li><a href="products.php?var=Computers">Computers</a></li>
                                <li><a href="products.php?var=Laptops">Laptops</a></li>
								<li><a href="products.php?var=Kids">Kids</a></li>
								<li><a href="products.php?var=Health&Beauty">Health&Beauty</a></li>
                                
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="search_box pull-right">
							 <form id="search" action="search.php" method="post">
							<input type="text" id="search" name="search" placeholder="Search by Name Or Category"/>
                            </form>
						</div>
					</div>
				</div>
			</div>
		</div>
    
    <!--/header-Navigation End-->
	</header>
    <!--/header-->
	 
     <!--contact-page-->
	 <div id="contact-page" class="container">
    	<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Contact Us</li>
				</ol>
			</div>   	
    		<div class="row">  	
	    		<div class="col-sm-8">
	    			<div class="contact-form">
                      <span>
   <?php 
   if(isset($_GET['msg']))
  echo "<p style='color:#3CF'> ".$_GET['msg']." </p>";
  ?>
  </span>
	    				<h2 class="title text-center">Get In Touch</h2>
	    				<div class="status alert alert-success" style="display: none"></div>
				    	<form id="main-contact-form" action="feedback.php" class="contact-form row" name="contact-form" method="post">
				            <div class="form-group col-md-6">
				                <input type="text" name="name" id="name" class="form-control" required placeholder="Name">
				            </div>
				            <div class="form-group col-md-6">
				                <input type="email" name="email" id="email" class="form-control" required placeholder="Email">
				            </div>
				            <div class="form-group col-md-12">
				                <input type="text" name="subject" id="subject" class="form-control" required placeholder="Subject">
				            </div>
				            <div class="form-group col-md-12">
				                <textarea name="message" id="message" required class="form-control" rows="8" placeholder="Your Message Here"></textarea>
				            </div>                        
				            <div class="form-group col-md-12">
				                <input type="submit" name="submit" class="btn btn-primary pull-right" value="Submit">
				            </div>
				        </form>
	    			</div>
	    		</div>
	    		<div class="col-sm-4">
	    			<div class="contact-info">
	    				<h2 class="title text-center">Contact Info</h2>
	    				<address>
	    					<p>LUXURY GIFTS</p>
							
							<p>UNITED KINGDOM</p>
							
							<p>Email: info@luxurygifts.co.uk</p>
	    				</address>
	    				<div class="social-networks">
	    					<h2 class="title text-center">Social Networking</h2>
							<ul>
								<li>
									<a href="#"><i class="fa fa-facebook"></i></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-twitter"></i></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-google-plus"></i></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-youtube"></i></a>
								</li>
							</ul>
	    				</div>
	    			</div>
    			</div>    			
	    	</div>  
    	</div>	
    </div>
    <!--/#contact-page-->
	
	        <!--Footer-->
            <?php include 'include/footer.php';?> 
            <!--/Footer-->
  
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>

</body>
</html>