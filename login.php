<?php
session_start();
include 'include/config.php';
?>

<!DOCTYPE html>
<html lang="en">

<!--head-->
<head>
<title>Login | LUXURY GIFTS</title>
<?php include 'include/head.php';?>    
</head>
<!--/head-->

<body>
	 <!--header-->
	<header id="header">
    
    <!--header Top-->
    <?php include 'include/headertop.php';?> 
    <!--/header Top-->
	     
       <!--header-Navigation--> 
		<div class="header-bottom">
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="index.php">Home</a></li>
								
                               
                                <li class="dropdown"><a href="#">Women's<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="products.php?var=Women Clothing">Clothing</a></li>
										<li><a href="products.php?var=Women Shoes">Shoes</a></li>
                                        <li><a href="products.php?var=Women Accessories">Accessories</a></li>
                                    </ul>
                                </li> 
                                
                                <li class="dropdown"><a href="#">Men's<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="products.php?var=Men Clothing">Clothing</a></li>
                                        <li><a href="products.php?var=Men Shoes">Shoes</a></li>
										<li><a href="products.php?var=Men Accessories">Accessories</a></li>
                                    </ul>
                                </li> 
                                 <li class="dropdown"><a href="#">Mobile&Tablets <i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="products.php?var=Phones">Phones</a></li>
										<li><a href="products.php?var=Tablets">Tablets</a></li> 
										
                                    </ul>
                                </li> 
                                <li><a href="products.php?var=Computers">Computers</a></li>
                                <li><a href="products.php?var=Laptops">Laptops</a></li>
								<li><a href="products.php?var=Kids">Kids</a></li>
								<li><a href="products.php?var=Health&Beauty">Health&Beauty</a></li>
                                
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="search_box pull-right">
							 <form id="search" action="search.php" method="post">
							<input type="text" id="search" name="search" placeholder="Search by Name Or Category"/>
                            </form>
						</div>
					</div>
				</div>
			</div>
		</div>
    
    <!--/header-Navigation End-->
	</header>
    <!--/header-->
	
	<section id="form"><!--form-->
		<div class="container">
        
			<div class="row">
				<div class="col-sm-4 col-sm-offset-1">
					<div class="login-form"><!--login form-->
						<h2>
                        <span>
   <?php 
   if(isset($_GET['msg']))
  echo "<p style='color:blue'> ".$_GET['msg']." </p>";
  ?>
  </span>
                        Login to your account
                        </h2>
                        <label>
<?php 
if (isset($_GET['registration']) && $_GET['registration']=='unavailable')
echo "<p style='color:red;'>Username and Password Invalid Try Again </p>";

?>
</label>
                        
						<form action="loginquery.php" method="post">
							<input type="text" id="username" name="username" placeholder="Enter Username" />
							<input type="password" id="password" name="password" placeholder="Enter Pasword" />
							<span>
								<input type="checkbox" class="checkbox"> 
								Keep me signed in
							</span>
                            <input type="hidden" id="id" name="id" value="<?php echo $id; ?>">
							<button type="submit" class="btn btn-default">Login</button>
						</form>
					</div><!--/login form-->
				</div>
				<div class="col-sm-1">
					<h2 class="or">OR</h2>
				</div>
				<div class="col-sm-4">
					<div class="login-form"><!--sign up form-->
						<h2>New User Signup!</h2>
                       <h2 style="font-weight:400">GET STARTED NOW <br/>
                        ITS FAST AND EASY
                        </h2>
						<a href="signup.php" id="signupbtn1">Sign Up Now</a>
					</div><!--/sign up form-->
				</div>
			</div>
		</div>
	</section><!--/form-->
	
	
	        <!--Footer-->
            <?php include 'include/footer.php';?> 
            <!--/Footer-->
 
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>