<?php
session_start();

include 'include/config.php';

include 'include/sessionchecker.php';

?>

<!DOCTYPE html>
<html lang="en">

<!--head-->
<head>
<title>My Account | LUXURY GIFTS</title>
<?php include 'include/head.php';?>    
</head>
<!--/head-->

<body>
	 <!--header-->
	<header id="header">
    
    <!--header Top-->
    <?php include 'include/headertop.php';?> 
    <!--/header Top-->
	     
        <!--header-Navigation--> 
		<div class="header-bottom">
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="index.php">Home</a></li>
								
                               
                                <li class="dropdown"><a href="#">Women's<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="products.php?var=Women Clothing">Clothing</a></li>
										<li><a href="products.php?var=Women Shoes">Shoes</a></li>
                                        <li><a href="products.php?var=Women Accessories">Accessories</a></li>
                                    </ul>
                                </li> 
                                
                                <li class="dropdown"><a href="#">Men's<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="products.php?var=Men Clothing">Clothing</a></li>
                                        <li><a href="products.php?var=Men Shoes">Shoes</a></li>
										<li><a href="products.php?var=Men Accessories">Accessories</a></li>
                                    </ul>
                                </li> 
                                 <li class="dropdown"><a href="#">Mobile&Tablets <i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="products.php?var=Phones">Phones</a></li>
										<li><a href="products.php?var=Tablets">Tablets</a></li> 
										
                                    </ul>
                                </li> 
                                <li><a href="products.php?var=Computers">Computers</a></li>
                                <li><a href="products.php?var=Laptops">Laptops</a></li>
								<li><a href="products.php?var=Kids">Kids</a></li>
								<li><a href="products.php?var=Health&Beauty">Health&Beauty</a></li>
                                
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="search_box pull-right">
							 <form id="search" action="search.php" method="post">
							<input type="text" id="search" name="search" placeholder="Search by Name Or Category"/>
                            </form>
						</div>
					</div>
				</div>
			</div>
		</div>
    
    <!--/header-Navigation End-->
	</header>
    <!--/header-->

	<section id="cart_items">
		<div class="container">
        
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">My Account</li>
				</ol>
			</div><!--/breadcrums-->

			
			<div class="register-req">
				<p>Account Details and Recent Items Add to Cart</p>
			</div><!--/register-req-->

			<div class="shopper-informations">
				<div class="row">
					
					<div class="col-sm-5 clearfix">
						<div class="bill-to">
                             <span>
   <?php 
   if(isset($_GET['msg4']))
  echo "<p style='color:#3CF'> ".$_GET['msg4']." </p>";
  ?>
  </span>
							<p>Personal Details</p>
							<div class="form-one">
                            
                              <?php

                        $username=  $_SESSION['username'];
                        $results = $mysqli->query("select * from users where username= '$username' ");	
						if ($results) { 
	
        //fetch results set as object and output HTML
        while($obj = $results->fetch_object())
        {
			
			?>
								<form>
									<input type="text" value="<?php echo $obj->email; ?>">
									<input type="text" value="<?php echo $obj->title; ?>">
									<input type="text"  value="<?php echo $obj->firstname; ?>">
									<input type="text"  value="<?php echo $obj->lastname; ?>">
                                    <input type="text" value="<?php echo $obj->username; ?>">
									<input type="text" value="<?php echo $obj->add1; ?>">
									
								</form>
							</div>
							<div class="form-two">
								<form>
                                    <input type="text" value="<?php echo $obj->add2; ?>">
									<input type="text" value="<?php echo $obj->postcode; ?>">
                                    <input type="text" value="<?php echo $obj->state; ?>">
									<input type="text" value="<?php echo $obj->phone; ?>">
									<input type="text" value="<?php echo $obj->mobile; ?>">
                             <a href="accountedit.php?id=<?php echo $obj->id; ?>" >       <button type="button" id="signupbtn">Edit Account</button></a>
									
								</form>
                          <?php   
                    }
    
                    }
	               ?>         
                                
                                
							</div>
						</div>
					</div>
								
				</div>
			</div>
			<div class="review-payment">
				<h2>Recent Items Added to Cart & Checkout</h2>
                   <span>
   <?php 
   if(isset($_GET['msg']))
  echo "<p style='color:red'> ".$_GET['msg']." </p>";
  ?>
  </span>
			</div>

			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Products Image</td>
							<td class="price">Product Name</td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						
                        
                        
                       <?php

                        $username=  $_SESSION['username'];
                        $results = $mysqli->query("select * from usercart where username= '$username' ");	
						if ($results) { 
	
        //fetch results set as object and output HTML
        while($obj = $results->fetch_object())
        {
			
			?>
                            					
						<tr>
							<td class="cart_product">
								<a href=""><img src="admin/images/<?php echo $obj->img; ?>" alt="" width="150" height="150"></a>
							</td>
							<td class="cart_price">
								<p><?php echo $obj->ptitle; ?></p>
							</td>
							<td class="cart_price">
								<p>$<?php echo $obj->price; ?></p>
							</td>
							<td class="cart_quantity">
								<p> <?php echo $obj->quantity; ?></p>
							</td>
							<td class="cart_total">
								<p class="cart_total_price">$<?php echo $obj->totalcart; ?></p>
							</td>
							<td class="cart_delete">
								<a class="cart_quantity_delete" href="delete2.php?id='<?php echo $obj->id; ?>'"><i class="fa fa-times"></i></a>
							</td>
						</tr>
                        
                          <?php   
                    }
    
                    }
	               ?>
                        
                        
                         
                         <?php
		
		$con=mysqli_connect("luxury1.db.11366411.hostedresource.com","luxury1","Luxury@123","luxury1");
        if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }
		$result1 = mysqli_query($con,"SELECT sum(totalcart),sum(points),sum(quantity) FROM usercart WHERE `username` = '$username'");
		
		
		while ($rows = mysqli_fetch_array($result1)) {
			
			$quatity = $rows['sum(quantity)'];
			$finaltotal= $rows['sum(totalcart)'];
			$totalpoints= $rows['sum(points)'];
			
			if ($totalpoints <  "100") {
			  $percent = '10';
			 $discount_value = ($finaltotal / 100) * $percent;
            
            } else if ($points <  "200") {
			  $percent = '20';
			  $discount_value = ($finaltotal / 100) * $percent;
          
            } else {
             $discount_value = '0';
            }
			
			$new_price = $finaltotal - $discount_value;
            ?>
						<tr>
							<td colspan="4">&nbsp;</td>
							<td colspan="2">
								<table class="table table-condensed total-result">
									<tr>
										<td>Cart Sub Total</td>
										<td>£<?php echo $finaltotal; ?></td>
									</tr>
                                    
									<tr>
										<td>Total Reward Points</td>
										<td><?php echo $totalpoints ; ?></td>
									</tr>
                                    
                                    <tr>
										<td>Discount <?php echo $percent; ?> %</td>
										<td>£<?php echo $discount_value; ?></td>
									</tr>
									
									<tr>
										<td>Total</td>
										<td><span>£<?php echo $new_price; ?></span></td>
									</tr>
                                      <?php }

	?>
                                    
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="payment-options">
					<span>
						<label><a class="btn btn-default check_out" href="https://www.paypal.com/uk/cgi-bin/webscr?cmd=_login-run">Check Out with PayPal</a></label>
					</span>
					
				</div>
		</div>
	</section> <!--/#cart_items-->

	

	         <!--Footer-->
            <?php include 'include/footer.php';?> 
            <!--/Footer-->
  
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>