<?php
session_start();

include 'include/config.php';

include 'include/sessionchecker.php';

?>

<!DOCTYPE html>
<html lang="en">
<!--head-->
<head>
<title>My Cart | LUXURY GIFTS</title>
<?php include 'include/head.php';?>    
</head>
<!--/head-->

<body>
	 <!--header-->
	<header id="header">
    
    <!--header Top-->
    <?php include 'include/headertop.php';?> 
    <!--/header Top-->
	     
       <!--header-Navigation--> 
		<div class="header-bottom">
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="index.php">Home</a></li>
								
                               
                                <li class="dropdown"><a href="#">Women's<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="products.php?var=Women Clothing">Clothing</a></li>
										<li><a href="products.php?var=Women Shoes">Shoes</a></li>
                                        <li><a href="products.php?var=Women Accessories">Accessories</a></li>
                                    </ul>
                                </li> 
                                
                                <li class="dropdown"><a href="#">Men's<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="products.php?var=Men Clothing">Clothing</a></li>
                                        <li><a href="products.php?var=Men Shoes">Shoes</a></li>
										<li><a href="products.php?var=Men Accessories">Accessories</a></li>
                                    </ul>
                                </li> 
                                 <li class="dropdown"><a href="#">Mobile&Tablets <i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="products.php?var=Phones">Phones</a></li>
										<li><a href="products.php?var=Tablets">Tablets</a></li> 
										
                                    </ul>
                                </li> 
                                <li><a href="products.php?var=Computers">Computers</a></li>
                                <li><a href="products.php?var=Laptops">Laptops</a></li>
								<li><a href="products.php?var=Kids">Kids</a></li>
								<li><a href="products.php?var=Health&Beauty">Health&Beauty</a></li>
                                
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="search_box pull-right">
							 <form id="search" action="search.php" method="post">
							<input type="text" id="search" name="search" placeholder="Search by Name Or Category"/>
                            </form>
						</div>
					</div>
				</div>
			</div>
		</div>
    
    <!--/header-Navigation End-->
	</header>
    <!--/header-->
    
	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Shopping Cart</li>
				</ol>
			</div>
			<div class="table-responsive cart_info">
             <span>
   <?php 
   if(isset($_GET['msg']))
  echo "<p style='color:red'> ".$_GET['msg']." </p>";
  ?>
  </span>
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Products Image</td>
							<td class="price">Product Name</td>
							<td class="price">Price</td>
                            <td class="price">Reward Points</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
							
                   <?php

                        $username=  $_SESSION['username'];
                        $results = $mysqli->query("select * from usercart where username= '$username' ORDER BY id desc ");	
						if ($results) { 
	
        //fetch results set as object and output HTML
        while($obj = $results->fetch_object())
        {
			$points = $obj->points;
						
			?>
                            					
						<tr>
							<td class="cart_product">
								<a href=""><img src="admin/images/<?php echo $obj->img; ?>" alt="" width="150" height="150"></a>
							</td>
							<td class="cart_price">
								<p><?php echo $obj->ptitle; ?></p>
							</td>
							<td class="cart_price">
								<p>£<?php echo $obj->price; ?></p>
							</td>
                            <td class="cart_price">
								<p><?php echo $points; ?></p>
							</td>
							<td class="cart_quantity">
								<p> <?php echo $obj->quantity; ?></p>
							</td>
							<td class="cart_total">
								<p class="cart_total_price">£<?php echo $obj->totalcart; ?></p>
							</td>
							<td class="cart_delete">
								<a class="cart_quantity_delete" href="delete.php?id='<?php echo $obj->id; ?>'"><i class="fa fa-times"></i></a>
							</td>
						</tr>
                        
                          <?php   
                    }
    
                    }
	               ?>
                        
					</tbody>
				</table>
			</div>
		</div>
	</section> <!--/#cart_items-->

	<section id="do_action">
		<div class="container">
			
			<div class="row">
				
				<div class="col-sm-6">
					<div class="total_area">
                    
        <?php
		
		$con=mysqli_connect("luxury1.db.11366411.hostedresource.com","luxury1","Luxury@123","luxury1");
        if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }
		$result1 = mysqli_query($con,"SELECT sum(totalcart),sum(points),sum(quantity) FROM usercart WHERE `username` = '$username'");
		
		
		while ($rows = mysqli_fetch_array($result1)) {
			
			$quatity = $rows['sum(quantity)'];
			$finaltotal= $rows['sum(totalcart)'];
			$totalpoints= $rows['sum(points)'];
			
			if ($totalpoints <=  "100") {
			  $percent = '10';
			 $discount_value = ($finaltotal / 100) * $percent;
            
            } else if ($points <=  "200") {
			  $percent = '20';
			  $discount_value = ($finaltotal / 100) * $percent;
          
			}
			else {
             $discount_value = '0';
            }
			
			$new_price = $finaltotal - $discount_value;
            ?>
                    
						<ul>
							<li>Cart Sub Total <span>£<?php echo $finaltotal; ?></span></li>
							<li>Rewards Points <span><?php echo $totalpoints ; ?></span></li>
                            <li>Discount <?php echo $percent; ?>% <span>£<?php echo $discount_value; ?></span></li>							
							<li>Total <span>£<?php echo $new_price; ?></span></li>
						</ul>
                        
                        <?php }

	?>             
    <form action="orderquery.php" method="post" id="send">
    <a class="btn btn-default check_out" href="index.php">Continue Shopping</a>
							<input type="hidden" value="<?php echo $username; ?>" name="username" id="username" />
                            <input type="hidden" value="<?php echo $quatity; ?>" name="quantity" id="quantity" />
                            <input type="hidden" value="<?php echo $totalpoints; ?>" name="points" id="points" />
                            <input type="hidden" value="<?php echo $percent; ?>" name="percent" id="percent" />
                            <input type="hidden" value="<?php echo $discount_value; ?>" name="discount_p" id="discount_p" />
                            <input type="hidden" value="<?php echo $new_price; ?>" name="total" id="total" />
                            <input type="submit" value="Proceed To Check out" class="btn btn-default check_out" />
                            </form>
    
                        
                        
							
							
					</div>
				</div>
			</div>
		</div>
	</section><!--/#do_action-->

	        <!--Footer-->
            <?php include 'include/footer.php';?> 
            <!--/Footer-->
  
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>