<?php
session_start();

include 'include/config.php';

?>

<!DOCTYPE html>
<html lang="en">

<!--head-->
<head>
<title>Products | LUXURY GIFTS</title>
<?php include 'include/head.php';?>    
</head>
<!--/head-->

<body>
	 <!--header-->
	<header id="header">
    
    <!--header Top-->
    <?php include 'include/headertop.php';?> 
    <!--/header Top-->
	     
        <!--header-Navigation--> 
		<div class="header-bottom">
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="index.php">Home</a></li>
								
                               
                                <li class="dropdown"><a href="#">Women's<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="products.php?var=Women Clothing">Clothing</a></li>
										<li><a href="products.php?var=Women Shoes">Shoes</a></li>
                                        <li><a href="products.php?var=Women Accessories">Accessories</a></li>
                                    </ul>
                                </li> 
                                
                                <li class="dropdown"><a href="#">Men's<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="products.php?var=Men Clothing">Clothing</a></li>
                                        <li><a href="products.php?var=Men Shoes">Shoes</a></li>
										<li><a href="products.php?var=Men Accessories">Accessories</a></li>
                                    </ul>
                                </li> 
                                 <li class="dropdown"><a href="#">Mobile&Tablets <i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="products.php?var=Phones">Phones</a></li>
										<li><a href="products.php?var=Tablets">Tablets</a></li> 
										
                                    </ul>
                                </li> 
                                <li><a href="products.php?var=Computers">Computers</a></li>
                                <li><a href="products.php?var=Laptops">Laptops</a></li>
								<li><a href="products.php?var=Kids">Kids</a></li>
								<li><a href="products.php?var=Health&Beauty">Health&Beauty</a></li>
                                
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="search_box pull-right">
							 <form id="search" action="search.php" method="post">
							<input type="text" id="search" name="search" placeholder="Search by Name Or Category"/>
                            </form>
						</div>
					</div>
				</div>
			</div>
		</div>
    
    <!--/header-Navigation End-->
	</header>
    <!--/header-->
	
	<section>
		<div class="container">
			<div class="row">
            
			<!--Side Navigation-->
            <?php include 'include/sidenavigation.php';?> 
            <!--/Side Navigation-->
				
				<div class="col-sm-9 padding-right">
                
                <!--features_items-->
                
                
			<div class="features_items">
            <?php
			
			 $var=$_GET['var'];
			?>
			<h2 class="title text-center"><?php echo $var ?></h2>
			 <div class="tab-pane fade active in" id="tshirt" >
			  
              <?php

   
	$results = $mysqli->query("SELECT * FROM products WHERE category = '$var' AND availability = 'In stock'");
    if ($results) { 
	
        //fetch results set as object and output HTML
        while($obj = $results->fetch_object())
        {
			echo '
			 <div class="col-sm-3">
				<div class="product-image-wrapper">
			<div class="single-products">
				   <div class="productinfo text-center">
			<img src="admin/images/'.$obj->img.'" alt="" />
					<h2>£'.$obj->price.'</h2>
					<p>'.$obj->title.'</p>
					<a href="productdetails.php?id='.$obj->id.'" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
					</div>
				   </div>
			</div>
			</div>
			
			
			';
        }
    
    }
    ?>  
            
            
            
		</div>
				</div>
						</div>
					</div>
                    <!--/category-tab-->
                    <!--features_items-->
				</div>
			</div>
		</div>
	</section>
	
	        <!--Footer-->
            <?php include 'include/footer.php';?> 
            <!--/Footer-->
  
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>