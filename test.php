<!-- 1. Take your Campaign Monitor subscribe form as generated from within your account: -->

<form action="http://myaccount.createsend.com/t/r/s/aljhk/" method="post" id="subForm">
    <div>
        <label for="name">Name:</label><br /><input type="text" name="cm-name" id="name" /><br />
        <label for="aljhk-aljhk">Email:</label><br /><input type="text" name="cm-aljhk-aljhk" id="aljhk-aljhk" /><br />
        <input type="submit" value="Subscribe" />
    </div>
</form>

<!-- 2. Add some JavaScript -->

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script> 
<script type="text/javascript">
    $(function () {
        $('#subForm').submit(function (e) {
            e.preventDefault();
            $.getJSON(
            this.action + "?callback=?",
            $(this).serialize(),
            function (data) {
                if (data.Status === 400) {
                    alert("Error: " + data.Message);
                } else { // 200
                    alert("Success: " + data.Message);
                }
            });
        });
    });
</script>